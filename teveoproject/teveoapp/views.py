import json
import random
import string
from django.shortcuts import render
import xml.etree.ElementTree as ET
import requests
from .models import *
from django.views.decorators.csrf import ensure_csrf_cookie
from datetime import datetime
from django.core.paginator import Paginator
from django.http import JsonResponse, HttpResponse
import base64


def SetCookie(request,resp):
    try:
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
    except:
        sesion = None
    if sesion is None:
        cookie = ''.join(random.choices(string.ascii_lowercase+string.digits,k=128))
        resp.set_cookie("cookie", cookie)
        sesion = Sesion(sesion_id=cookie)
        sesion.save()
    return sesion

def get_config(request):
    try:
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
    except:
        sesion = None
    if sesion is None:
        tipo = "sans_serif"
        tam = "medium"
        nombre = "Anonimo"
    else:
        tipo = sesion.tipoLetra
        tam = "medium"
        if tipo == "default":
            tipo = "sans-serif"
        tama = sesion.tamLetra
        if tama == 3:
            tam = "medium"
        elif tama == 8:
            tam = "small"
        elif tama == 12:
            tam = "medium"
        elif tama == 16:
            tam = "large"
        nombre = sesion.usuario
    return tipo,tam,nombre

def footer(request):
    camaras = Camara.objects.all()
    num_camaras = len(camaras)
    comentarios = Comentario.objects.all()
    num_comentarios = len(comentarios)
    return num_camaras, num_comentarios

@ensure_csrf_cookie
def principal(request):
    tipo,tam,nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    comentarios = Comentario.objects.all().order_by("-fecha")
    comentarios = list(comentarios)
    for comentario in comentarios:
        comentario.img= base64.b64encode(comentario.img).decode('utf-8')
    paginator = Paginator(comentarios, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'page_obj' : page_obj,
        'tipo':tipo,
        'tam':tam,
        'nombre_usuario':nombre_usuario,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios
    }
    resp = render(request, "principal.html", context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()
    if 'boton' in request.POST:
        if request.POST["boton"] == "cerrar_sesion":
            sesion = Sesion.objects.filter(sesion_id=request.COOKIES.get("cookie"))
            if sesion:
                sesion.delete()
                resp.delete_cookie("cookie")
    return resp

@ensure_csrf_cookie
def configuracion(request):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    context = {
        'tipo': tipo,
        'tam': tam,
        'nombre_usuario': nombre_usuario,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios
    }
    resp = render(request, "configuracion.html",context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()
    if request.method == "POST":
        nombre = request.POST["nombre"]
        tipo = request.POST["tipo"]
        tam = request.POST["tamano"]
        sesion = request.COOKIES.get("cookie")
        sesion = Sesion.objects.get(sesion_id=sesion)
        sesion.usuario = nombre
        sesion.tipoLetra = tipo
        sesion.tamLetra= int(tam)
        sesion.save()
        tipo, tam, nombre_usuario = get_config(request)
        context = {
            'tipo': tipo,
            'tam': tam,
            'nombre_usuario': nombre_usuario,
            'num_camaras': num_camaras,
            'num_comentarios': num_comentarios
        }
        resp = render(request, "configuracion.html", context)
    return resp

def ayuda(request):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    context = {
        'tipo': tipo,
        'tam': tam,
        'nombre_usuario': nombre_usuario,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios
    }
    resp = render(request, "ayuda.html", context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()
    return resp

@ensure_csrf_cookie
def comentario(request):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    id = request.GET.get('camara')
    context = {}
    camara = Camara.objects.get(camara_id=id)
    fecha = datetime.now()
    fecha = fecha.strftime("%d/%m/%Y, %H:%M:%S")
    context['id'] = id
    context['camara'] = camara
    context['fecha'] = fecha
    context['tipo'] = tipo
    context['tam'] = tam
    context['nombre_usuario'] = nombre_usuario
    context['num_camaras'] = num_camaras
    context['num_comentarios'] = num_comentarios
    resp = render(request,"comentario.html",context)

    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()

    if request.method == "POST":
        sesion = request.COOKIES.get("cookie")
        sesion = Sesion.objects.get(sesion_id=sesion)
        autor = sesion.usuario
        texto = request.POST["text"]
        id_camara = request.POST["camara"]
        url_img = request.POST["image"]

        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36'
        }
        respuesta_img = requests.get(url_img, headers=headers)
        camara = Camara.objects.get(camara_id=id_camara)
        comentario = Comentario(id_camara=camara,autor=autor,texto=texto,img=respuesta_img.content, fecha=timezone.now())
        comentario.save()
        camara.num_comentarios += 1
        camara.save()

    return resp

def datos_list1(ruta):
    list_camaras = []
    tree = ET.parse(ruta)
    root = tree.getroot()
    existe_camara = root.findall('camara')
    if existe_camara:
        for cam in existe_camara:
            list_cam = []
            id = cam.find('id').text
            url = cam.find('src').text
            nombre = cam.find('lugar').text
            localizacion = cam.find('coordenadas').text
            list_cam.append(id)
            list_cam.append(nombre)
            list_cam.append(url)
            list_cam.append(localizacion)
            list_camaras.append(list_cam)
    return list_camaras

def datos_list2(ruta):
    list_camaras = []
    tree = ET.parse(ruta)
    root = tree.getroot()
    existe_cam = root.findall('cam')
    if existe_cam:
        for cam in existe_cam:
            list_cam = []
            id = cam.get('id')
            url = cam.find('url').text
            nombre = cam.find('info').text
            localizacion = cam.find('place')
            latitud = localizacion.find('latitude').text
            longitud = localizacion.find('longitude').text
            localizacion = latitud + ", " + longitud
            list_cam.append(id)
            list_cam.append(nombre)
            list_cam.append(url)
            list_cam.append(localizacion)
            list_camaras.append(list_cam)
    return list_camaras

@ensure_csrf_cookie
def camara(request, camara):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    camara = Camara.objects.get(camara_id=camara)

    comentarios = Comentario.objects.filter(id_camara=camara).order_by("-fecha")
    comentarios = list(comentarios)

    num_votos = Voto.objects.filter(camara=camara)
    num_votos = len(num_votos)

    context = {
        'camara': camara,
        'comentarios': comentarios,
        'tipo': tipo,
        'tam': tam,
        'nombre_usuario': nombre_usuario,
        'num_camaras': num_camaras,
        'num_comentarios':num_comentarios,
        'votos': num_votos
    }
    resp = render(request,"camara.html",context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()

    if request.method == 'POST':
        sesion = Sesion.objects.get(sesion_id=request.COOKIES.get("cookie"))
        usuario = sesion.usuario
        voto = Voto(camara=camara, usuario=usuario)
        voto.save()
        num_votos = Voto.objects.filter(camara=camara)
        num_votos = len(num_votos)
        context['votos'] = num_votos
        resp = render(request, "camara.html", context)
    return resp

def all_camaras(request):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    if 'boton' in request.POST:
        listado = []
        identificadores = Camara.objects.values_list('camara_id', flat=True)
        identificadores = list(identificadores)
        if request.POST['boton'] == 'listado1':
            ruta = '/home/alumnos/bernabeu/Escritorio/Final-TeVeo/final-teveo/listado1.xml'
            listado = datos_list1(ruta)
        elif request.POST['boton'] == 'listado2':
            ruta = '/home/alumnos/bernabeu/Escritorio/Final-TeVeo/final-teveo/listado2.xml'
            listado = datos_list2(ruta)
        for i in range(0, len(listado)):
            id = listado[i][0]
            if id not in identificadores:
                nombre = listado[i][1]
                url = listado[i][2]
                localizacion = listado[i][3]
                camara = Camara(camara_id=id, nombre=nombre, url=url, localizacion=localizacion)
                camara.save()
    urls = list(Camara.objects.values_list('url', flat=True))
    if len(urls) == 0:
        imagen_aleatoria = ""
    else:
        imagen_aleatoria = random.choice(urls)
    camaras = Camara.objects.order_by('-num_comentarios')
    paginator = Paginator(camaras, 5)
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    votos = {}
    coment = {}
    for cam in camaras:
        num_votos = Voto.objects.filter(camara=cam)
        num_votos = len(num_votos)
        votos[cam.camara_id] = num_votos
        num_com = Comentario.objects.filter(id_camara=cam)
        num_com = len(num_com)
        coment[cam.camara_id] =num_com

    context = {
        'imagen_aleatoria' : imagen_aleatoria,
        'page_obj' : page_obj,
        'nombre_usuario': nombre_usuario,
        'tipo': tipo,
        'tam': tam,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios,
        'lista_num_votos': votos.items(),
        'lista_num_com': coment.items()
    }

    resp = render(request, "camaras.html", context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()
    return resp

def camara_dinamica(request, nombre):
    tipo, tam, nombre_usuario = get_config(request)
    num_camaras, num_comentarios = footer(request)
    camara = Camara.objects.get(camara_id=nombre)
    comentarios = Comentario.objects.filter(id_camara=camara).order_by("-fecha")
    comentarios = list(comentarios)

    num_votos = Voto.objects.filter(camara=camara)
    num_votos = len(num_votos)

    context = {
        'camara': camara,
        'comentarios': comentarios,
        'tipo': tipo,
        'tam': tam,
        'nombre_usuario': nombre_usuario,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios,
        'votos': num_votos
    }
    resp = render(request, "camara_dinamica.html", context)
    sesion = request.COOKIES.get("cookie")
    if sesion is None:
        sesion = SetCookie(request, resp)
        sesion = Sesion.objects.get(sesion_id=sesion.sesion_id)
        sesion.save()
    return resp

def camara_json(request, camara):
    camara = Camara.objects.get(camara_id=camara)
    num_com = Comentario.objects.filter(id_camara=camara)
    num_com = len(num_com)
    datos_camara = {
            "Id":camara.camara_id,
            "Nombre":camara.nombre,
            "Localizacion":camara.localizacion,
            "Imagen":camara.url,
            "Comentarios":num_com
        }
    datos_camara=json.dumps(datos_camara)
    return HttpResponse(datos_camara, content_type='application/json') #ver como hacer en JSON en mi práctica del año pasado