from django.db import models
from django.utils import timezone

class Sesion(models.Model):
    usuario = models.CharField(max_length=30, default="Anónimo")
    sesion_id = models.TextField(unique=True)
    tipoLetra = models.TextField(default="default")
    tamLetra = models.IntegerField(default="3")

class Camara(models.Model):
    camara_id = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    url = models.URLField()
    localizacion = models.TextField()
    num_comentarios = models.IntegerField(default=0)

    def __str__(self):
        return self.nombre

class Comentario(models.Model):
    id_camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    autor = models.TextField()
    texto = models.TextField()
    fecha = models.DateTimeField(default=timezone.now())
    img = models.BinaryField()

class Voto(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    usuario = models.TextField()

