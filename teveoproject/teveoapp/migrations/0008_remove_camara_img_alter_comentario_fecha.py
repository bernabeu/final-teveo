# Generated by Django 5.0.3 on 2024-06-19 18:56

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("teveoapp", "0007_camara_comentario_alter_comentario_fecha"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="camara",
            name="img",
        ),
        migrations.AlterField(
            model_name="comentario",
            name="fecha",
            field=models.DateTimeField(
                default=datetime.datetime(
                    2024, 6, 19, 18, 56, 0, 781459, tzinfo=datetime.timezone.utc
                )
            ),
        ),
    ]
