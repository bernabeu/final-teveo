from django.urls import path
from . import views

urlpatterns=[
    path("",views.principal,name='principal'),
    path("configuracion", views.configuracion),
    path("ayuda",views.ayuda),
    path("comentario",views.comentario),
    path("all",views.all_camaras),
    path("<str:nombre>-dyn",views.camara_dinamica),
    path("<str:camara>.json",views.camara_json),
    path("<str:camara>",views.camara)
]