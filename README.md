

# ENTREGA CONVOCATORIA JUNIO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Alejandro Bernabéu Fernández
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: bernabeu
* Cuenta URJC: a.bernabeu.2018@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=3v6Zwh1G2oA
* Video parte opcional (url): https://www.youtube.com/watch?v=Vix3xGx58nA
* Despliegue (url): http://bernabeu04.pythonanywhere.com/
* Contraseñas: no se necesitan contraseñas para acceder al sitio
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria


* PÁGINA PRINCIPAL

La página principal muestra una lista de comentarios ordenados del más reciente al más antiguo. Cada comentario incluye información como el identificador de la cámara, la fecha del comentario, el texto del comentario y una imagen de la cámara tomada en el momento en que se hizo el comentario.

* PÁGINA DE CÁMARAS

La página de cámaras ofrece un listado de fuentes de datos disponibles. Cada fuente de datos tiene un botón para descargarla y almacenar la información en la base de datos. Además, se presenta una lista de todas las cámaras ordenadas por el número de comentarios, con una imagen aleatoria de una cámara en la parte superior. Para cada cámara se muestra su identificador, nombre, número de comentarios y enlaces tanto a la página de la cámara como a su página dinámica.

* PÁGINA DE UNA CÁMARA

La página individual de cada cámara proporciona información detallada sobre la cámara. Incluye enlaces a la página dinámica de la cámara, el nombre y la ubicación de la cámara, una imagen actual, un enlace para agregar un comentario y una lista de todos los comentarios asociados a esa cámara.

* PÁGINA PARA COMENTAR

La página para agregar un comentario recibe un parámetro que indica el identificador de la cámara. Muestra la información de la cámara, una imagen actual, la fecha y hora, y un formulario para ingresar el texto del comentario.

* PÁGINA DINÁMICA DE LA CÁMARA

La página dinámica de cada cámara se parece a la página individual de la cámara, pero utiliza HTMX para actualizar periódicamente la imagen y los comentarios cada 30 segundos. También permite cargar el formulario para agregar un comentario al hacer clic en el enlace correspondiente.

* PÁGINA DE CONFIGURACIÓN

La página de configuración permite personalizar el sitio para el navegador actual. Incluye un formulario para elegir el nombre con el que se publicarán los comentarios y opciones para personalizar la apariencia del sitio.

* PÁGINA DE AYUDA

La página de ayuda proporciona información sobre la práctica y su funcionamiento.

## Lista partes opcionales

* Logout: se introduce un botón que permita cerrar sesión, reinicie la configuración del usuario a los valores por defecto y elimine la sesión.
* Favicon: se introduce un favicon al sitio.
* Votar cámaras: se introduce un botón en la página de cada cámara para votarla.

## Anotaciones

Al desplegar la aplicación en PythonAnywhere no se permite comentar. Esto es a causa de que a la hora de capturar la imagen en el momento del mensaje, el servidor pide mediante un requests la url de la propia imagen. Al pedir esta url, salta un error 403 Forbbidden por no aparecer en la lista blanca. Por consiguiente, en la página principal no aparecen los comentarios, ya que al tener ese error no se puede guardar el comentario en la base de datos. Sin embargo, en el despliegue local esto no ocurre, y la aplicacion funciona correctamente.

En resumen, las urls de las imágenes tomadas en el momento del comentario no aparecen en la lista blanca de PythonAnywhere.
